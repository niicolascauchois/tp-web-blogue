<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\Categorie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @param int $nbArticle
     * @return Article[]
     */
    public function findLastArticle(int $nbArticle)
    {
        return $this->createQueryBuilder('a')
            ->setMaxResults($nbArticle)
            ->orderBy("a.dateDeCreation", "DESC")
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $nbArticle
     * @return Article[]
     */
    public function findMostVisiterArticle(int $nbArticle)
    {
        return $this->createQueryBuilder('a')
            ->setMaxResults($nbArticle)
            ->orderBy("a.nbVisite", "DESC")
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Categorie|null $categorie
     * @return QueryBuilder
     */
    public function getArticleQueryBuilder(Categorie $categorie = null): QueryBuilder
    {
        $qb = $this->createQueryBuilder('a')
            ->orderBy("a.dateDeCreation", "DESC");
        if ($categorie != null) {
            $qb->where("a.categorie = :categorie")
                ->setParameter('categorie', $categorie);
        }
        return $qb;
    }
}
