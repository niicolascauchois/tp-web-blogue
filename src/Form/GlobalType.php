<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;

class GlobalType extends AbstractType
{
    const LABEL = "label";
    const CONSTRAINTS = 'constraints';
}