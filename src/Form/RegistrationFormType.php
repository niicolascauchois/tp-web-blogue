<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends GlobalType
{
    private $contrainte;

    /**
     * RegistrationFormType constructor.
     */
    public function __construct()
    {
        $this->contrainte = [
            new NotBlank([
                'message' => 'Veuillez saisir mot de passe',
            ]),
            new Length([
                'min' => 6,
                'minMessage' => 'Le mot de passe doit faire {{ limit }} caractères minimum.',
                'max' => 4096,
                'maxMessage' => 'Le mot de passe doit faire {{ limit }} caractères maximum.',
            ]),
        ];
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                self::LABEL => "Email",
            ])
            ->add('agreeTerms', CheckboxType::class, [
                self::LABEL => "Accepter le contrat *",
                'mapped' => false,
                self::CONSTRAINTS => [
                    new IsTrue([
                        'message' => 'Vous devez accepter les conditions générales.',
                    ]),
                ],
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe ne corespondent pas.',
//                'mapped' => false,
                'first_options' => [
                    self::LABEL => 'Mots de passe',
                    self::CONSTRAINTS => $this->contrainte,
                ],
                'second_options' => [
                    self::LABEL => 'Répéter le mots de passe',
                    self::CONSTRAINTS => $this->contrainte,
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
