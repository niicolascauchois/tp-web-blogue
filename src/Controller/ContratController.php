<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

class ContratController extends GlobalController
{
    /**
     * @Route("/contrat", name="contrat")
     */
    public function index()
    {
        return $this->render('contrat/index.html.twig', [
            'controller_name' => 'ContratController',
        ]);
    }
}
