<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends GlobalController
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var PaginatorInterface
     */
    private $paginator;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * AdminController constructor.
     * @param UserRepository $userRepository
     * @param PaginatorInterface $paginator
     * @param EntityManagerInterface $em
     */
    public function __construct(UserRepository $userRepository, PaginatorInterface $paginator, EntityManagerInterface $em)
    {
        $this->userRepository = $userRepository;
        $this->paginator = $paginator;
        $this->em = $em;
    }

    /**
     * @Route("/admin", name="admin")
     * @IsGranted("ROLE_ADMIN")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        if ($request->isMethod('POST')) {
            $idUser = $request->request->getInt("idUser", -1);
            $filterValue = $request->request->get("filterValue");
            if ($idUser != -1) {
                $user = $this->userRepository->find($idUser);
                if ($user != null) {
                    if (in_array("ROLE_ADMIN", $user->getRoles())) {
                        $user->setRoles([]);
                    } else {
                        $user->setRoles(['ROLE_ADMIN']);
                    }
                    $this->em->persist($user);
                    $this->em->flush();
                }
            } elseif ($filterValue != null) {
                $queryBuilder = $this->userRepository->findUserByEmail($filterValue);
            }
        } else {
            $queryBuilder = $this->userRepository->findAll();
        }
        $pagination = $this->paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            5
        );
        return $this->render('admin/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}
