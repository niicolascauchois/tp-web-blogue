<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\CategorieRepository;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends GlobalController
{
    /**
     * @var CategorieRepository
     */
    private $categorieRepository;

    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * SlugGenerator constructor.
     * @param CategorieRepository $categorieRepository
     * @param ArticleRepository $articleRepository
     */
    public function __construct(CategorieRepository $categorieRepository, ArticleRepository $articleRepository)
    {
        $this->categorieRepository = $categorieRepository;
        $this->articleRepository = $articleRepository;
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'dernierArticle' => $this->articleRepository->findLastArticle(4),
            'plusVisterArticle' => $this->articleRepository->findMostVisiterArticle(4),
        ]);
    }
}
