<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class GlobalController extends AbstractController
{
    const ARTICLE_INDEX = 'article_index';
    const ARTICLE = 'article';
    const CATEGORIE = 'categorie';
    const CATEGORIE_INDEX = 'categorie_index';
    const FORM = 'form';
    const INDEX = 'index';
}