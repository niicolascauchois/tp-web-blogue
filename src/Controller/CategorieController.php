<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Form\CategorieType;
use App\Repository\ArticleRepository;
use App\Repository\CategorieRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/categorie")
 */
class CategorieController extends GlobalController
{

    /**
     * @Route("/", name="categorie_index", methods={"GET"})
     * @param CategorieRepository $categorieRepository
     * @return Response
     */
    public function index(CategorieRepository $categorieRepository): Response
    {
        return $this->render('categorie/index.html.twig', [
            'categories' => $categorieRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="categorie_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request): Response
    {
        $categorie = new Categorie();
        $form = $this->createForm(CategorieType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categorie);
            $entityManager->flush();

            return $this->redirectToRoute(self::CATEGORIE_INDEX);
        }

        return $this->render('categorie/new.html.twig', [
            self::CATEGORIE => $categorie,
            self::FORM => $form->createView(),
        ]);
    }

    /**
     * @Route("/{nom}", name="categorie_show", methods={"GET"})
     * @param Categorie $categorie
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param ArticleRepository $articleRepository
     * @return Response
     */
    public function show(Categorie $categorie, Request $request, PaginatorInterface  $paginator, ArticleRepository $articleRepository): Response
    {
        $queryBuilder = $articleRepository->getArticleQueryBuilder($categorie);
        $pagination = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            5
        );
        return $this->render('categorie/show.html.twig', [
            'pagination' => $pagination,
            self::CATEGORIE => $categorie,
        ]);
    }

    /**
     * @Route("/{nom}/edit", name="categorie_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Categorie $categorie
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Categorie $categorie): Response
    {
        $form = $this->createForm(CategorieType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute(self::CATEGORIE_INDEX);
        }

        return $this->render('categorie/edit.html.twig', [
            self::CATEGORIE => $categorie,
            self::FORM => $form->createView(),
        ]);
    }

    /**
     * @Route("/{nom}", name="categorie_delete", methods={"DELETE"})
     * @param Request $request
     * @param Categorie $categorie
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Categorie $categorie): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categorie->getNom(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($categorie);
            $entityManager->flush();
        }

        return $this->redirectToRoute(self::CATEGORIE_INDEX);
    }
}
