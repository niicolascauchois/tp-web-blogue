<?php
namespace App\Service;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
class Gravatar
{
    public function generate(string $email, int $size = 50): string
    {
        return "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $_ENV['AVATAR_DEFAUT'] ) . "&s=" . $size;
    }
}