<?php
namespace App\Service;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
class SlugGenerator
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;
    /**
     * SlugGenerator constructor.
     * @param ArticleRepository $articleRepository
     */
    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }
    public function generate(string $name): string
    {
        $oldLocale = setlocale(LC_ALL, '0');
        setlocale(LC_ALL, 'en_US.UTF-8');
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $name);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower($clean);
        $clean = preg_replace("/[\/_|+ -]+/", '-', $clean);
        $clean = trim($clean, '-');
        setlocale(LC_ALL, $oldLocale);
        $i = 0;
        while ($this->articleRepository->findBy(["slug" => (($i == 0) ? $clean : ($clean . "_" . $i))]) != []) {
            $i += 1;
        }
        return (($i == 0) ? $clean : ($clean . "_" . $i));
    }
}