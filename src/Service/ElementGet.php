<?php
namespace App\Service;
use App\Repository\ArticleRepository;
use App\Repository\CategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
class ElementGet
{
    /**
     * @var CategorieRepository
     */
    private $categorieRepository;
    /**
     * @var ArticleRepository
     */
    private $articleRepository;
    /**
     * SlugGenerator constructor.
     * @param CategorieRepository $categorieRepository
     * @param ArticleRepository $articleRepository
     */
    public function __construct(CategorieRepository $categorieRepository, ArticleRepository $articleRepository)
    {
        $this->categorieRepository = $categorieRepository;
        $this->articleRepository = $articleRepository;
    }
    public function getCategories()
    {
        return $this->categorieRepository->findAll();
    }
    public function getArticles()
    {
        return $this->articleRepository->findAll();
    }
}