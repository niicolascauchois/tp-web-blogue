<?php

namespace App\Service;

use App\Repository\ArticleRepository;
use App\Repository\CategorieRepository;
use Doctrine\ORM\EntityManagerInterface;

class ConvetHTMLToString
{
    /**
     * @param string $html
     * @param int|null $nbChar
     * @return string
     */
    public function convert(string $html, int $nbChar = null): string
    {
        $html = strip_tags($html);
        if ($nbChar != null && strlen($html) > $nbChar) {
            $html = substr($html, 0, $nbChar - 3);
            $html .= '...';
        }
        return $html;
    }
}