<?php

namespace App\Command;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AddAdminCommand extends AbstractAdminCommand
{
    protected static $defaultName = 'app:addAdmin';

    protected function configure()
    {
        $this
            ->setDescription('Permet de nomer un utilisateur administrateur')
            ->addOption('id', null, InputOption::VALUE_OPTIONAL, 'Id de l\'utilisateur')
            ->addOption('email', null, InputOption::VALUE_OPTIONAL, 'Email de l\'utilisateur')
            ->setHelp('Cette commande permet de nomer un utilisateur administrateur.
            Ex :
             - php bin/console app:addAdmin --id=1
             - php bin/console app:addAdmin --email=test@test.fr'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = parent::execute($input,$output);
        if ($this->user == null){
            return 1;
        }

        $this->user->setRoles(['ROLE_ADMIN']);
        $this->entityManager->persist($this->user);
        $this->entityManager->flush();
        $io->success('L\'utilisateur "'.$this->user->getEmail().'" a été reçu le role "ROLE_ADMIN".');

        return 0;
    }
}
