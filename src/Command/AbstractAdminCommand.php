<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class AbstractAdminCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @var User
     */
    protected $user;

    /**
     * AddAdminCommand constructor.
     * @param EntityManagerInterface $em
     * @param UserRepository $users
     */
    public function __construct(EntityManagerInterface $em, UserRepository $users)
    {
        parent::__construct();

        $this->entityManager = $em;
        $this->users = $users;
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return SymfonyStyle
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $id = $input->getOption('id');
        $email = $input->getOption('email');
        if ($id != null) {
            $this->user = $this->users->findOneBy(['id' => $id]);
            if ($this->user == null) {
                $io->error('L\'utilisateur n\'a pas été trouver avec l\'id "' . $id . '"');
            }
        } elseif ($email != null) {
            $this->user = $this->users->findOneBy(['email' => $email]);
            if ($this->user == null) {
                $io->error('L\'utilisateur n\'a pas été trouver avec l\'email "' . $email . '"');
            }
        } else {
            $io->error('Vous devez mêttre l\'opption id ou email. Pour avoir l\'aide --help.');
        }
        return $io;
    }
}
