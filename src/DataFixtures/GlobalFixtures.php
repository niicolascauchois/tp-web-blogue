<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

abstract class GlobalFixtures extends Fixture
{
//    Constante User
    const NB_USER = 10;
//    Constante Categorie
    const NB_CATEGORIE = 10;
//    Constante Article
    const NB_ARTICLES = 1000;
    const NB_ARTICLES_VISIT_MIN = 10;
    const NB_ARTICLES_VISIT_MAX = 1000;
    const NB_ARTICLES_PARAGRAPH_MIN = 5;
    const NB_ARTICLES_PARAGRAPH_MAX = 25;

    /** @var Generator */
    protected $faker;

    /**
     * AppFixtures constructor.
     */
    public function __construct()
    {
        $this->faker = Factory::create();;
    }

    abstract public function load(ObjectManager $manager);
}
