<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CategorieFixtures extends GlobalFixtures implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        dump("Création Categorie");
        for ($i = 0; $i < self::NB_CATEGORIE; $i++) {
            $categorie = new Categorie();
            $categorie->setNom($this->faker->word);
            $manager->persist($categorie);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['CategorieFixtures'];
    }
}
