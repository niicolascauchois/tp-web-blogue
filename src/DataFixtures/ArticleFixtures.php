<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Categorie;
use App\Repository\CategorieRepository;
use App\Repository\UserRepository;
use App\Service\SlugGenerator;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Generator;

class ArticleFixtures extends GlobalFixtures implements FixtureGroupInterface
{
    /**
     * @var SlugGenerator
     */
    private $slugGenerator;

    /**
     * @var CategorieRepository
     */
    private $categorieRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ArticleFixtures constructor.
     * @param SlugGenerator $slugGenerator
     * @param CategorieRepository $categorieRepository
     * @param UserRepository $userRepository
     */
    public function __construct(SlugGenerator $slugGenerator, CategorieRepository $categorieRepository, UserRepository $userRepository)
    {
        parent::__construct();
        $this->slugGenerator = $slugGenerator;
        $this->categorieRepository = $categorieRepository;
        $this->userRepository = $userRepository;
    }

    public function load(ObjectManager $manager)
    {
        dump("Création Article");
        for ($i = 0; $i < self::NB_ARTICLES; $i++) {
            $article = new Article();
            $article->setTitre($this->faker->text);
            $article->setSlug($this->slugGenerator->generate($article->getTitre()));
            $article->setNbVisite(mt_rand(self::NB_ARTICLES_VISIT_MIN, self::NB_ARTICLES_VISIT_MAX));
            $article->setDateDeCreation($this->faker->dateTime);
            $contenu = "";
            for ($y = 0; $y < mt_rand(self::NB_ARTICLES_PARAGRAPH_MIN, self::NB_ARTICLES_PARAGRAPH_MAX); $y++) {
                $contenu .= $this->faker->paragraph;
                $contenu .= "\n\n";
            }
            $article->setCategorie($this->categorieRepository->find(mt_rand(1, self::NB_CATEGORIE)));
            $article->setUser($this->userRepository->find(mt_rand(1, self::NB_USER)));
            $article->setContenu($contenu);
            $manager->persist($article);
        }

        $manager->flush();
    }




    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['ArticleFixtures'];
    }
}
