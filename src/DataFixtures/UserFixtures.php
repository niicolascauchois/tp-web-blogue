<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends GlobalFixtures implements FixtureGroupInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        parent::__construct();
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {

        dump("Création Utilisateur");
        $user = new User();
        $user->setEmail("admin@exemple.fr");
        $user->setPassword($this->encoder->encodePassword($user, 'admin'));
        $user->setRoles(["ROLE_ADMIN"]);
        $manager->persist($user);
        $manager->flush();

        $user = new User();
        $user->setEmail("test@exemple.fr");
        $user->setPassword($this->encoder->encodePassword($user, 'test'));
        $user->setRoles([]);
        $manager->persist($user);
        $manager->flush();

        for ($i = 0; $i < self::NB_USER - 2; $i++) {
            $user = new User();
            $user->setEmail($this->faker->email);
            $user->setPassword($this->encoder->encodePassword($user, $this->faker->password));
            $user->setRoles([]);
            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['UserFixtures'];
    }
}
