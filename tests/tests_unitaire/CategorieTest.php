<?php

namespace App\Tests\tests_unitaire;

use App\Entity\Article;
use App\Entity\Categorie;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;

class CategorieTest extends TestCase
{
    const NOM = "nom";
    /**
     * @var Categorie
     */
    private $categorie;

    /**
     * @var Article
     */
    private $article;

    /**
     * @var Collection
     */
    private $articleColectionEmpty;

    /**
     * @var Collection
     */
    private $articleColectionFull;

    public function setUp(): void
    {
        parent::setUp();
        $this->categorie = new Categorie();
        $this->article = new Article();
        $this->articleColectionEmpty = new ArrayCollection();
        $this->articleColectionFull = new ArrayCollection();
        $this->articleColectionFull[] = $this->article;

    }


    public function testToString()
    {
        $this->categorie->setNom(self::NOM);

        $this->assertEquals(self::NOM,$this->categorie->__toString());
    }


    public function testNom()
    {
        $this->categorie->setNom(self::NOM);

        $this->assertEquals(self::NOM,$this->categorie->getNom());
    }

    public function testArticles()
    {
        $this->assertEquals($this->categorie->getArticles(),$this->articleColectionEmpty);
        $this->categorie->addArticle($this->article);
        $this->assertEquals($this->categorie->getArticles(),$this->articleColectionFull);
        $this->categorie->removeArticle($this->article);
        $this->assertEquals($this->categorie->getArticles(),$this->articleColectionEmpty);
    }
}
