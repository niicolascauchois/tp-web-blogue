<?php

namespace App\Tests\tests_unitaire;

use App\Entity\Article;
use App\Entity\Categorie;
use App\Entity\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;

class ArticleTest extends TestCase
{
    const TITRE = "titre";
    const SLUG = "titre";
    const CONTENUS = "contenus";
    const NB_VISITE = 50;

    /**
     * @var Article
     */
    private $article;

    /**
     * @var Categorie
     */
    private $categorie;

    /**
     * @var User
     */
    private $user;

    /**
     * @var DateTime
     */
    private $date;

    public function setUp(): void
    {
        parent::setUp();
        $this->article = new Article();
        $this->categorie = new Categorie();
        $this->user = new User();
        $this->date = new DateTime();

    }

    public function testNbVisite()
    {
        $this->article->setNbVisite(self::NB_VISITE);

        $this->assertEquals(self::NB_VISITE, $this->article->getNbVisite());
    }

    public function testContenu()
    {
        $this->article->setContenu(self::CONTENUS);

        $this->assertEquals(self::CONTENUS, $this->article->getContenu());
    }

    public function testUser()
    {
        $this->article->setUser($this->user);

        $this->assertEquals($this->user, $this->article->getUser());
    }

    public function testCategorie()
    {
        $this->article->setCategorie($this->categorie);

        $this->assertEquals($this->categorie, $this->article->getCategorie());
    }

    public function testSlug()
    {
        $this->article->setSlug(self::SLUG);

        $this->assertEquals(self::SLUG, $this->article->getSlug());
    }

    public function testDateDeCreation()
    {
        $this->article->setDateDeCreation($this->date);

        $this->assertEquals($this->date, $this->article->getDateDeCreation());
    }

    public function testTitre()
    {
        $this->article->setTitre(self::TITRE);

        $this->assertEquals(self::TITRE, $this->article->getTitre());
    }
}
