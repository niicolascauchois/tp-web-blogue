<?php

namespace App\Tests\tests_fonctionel;

use App\DataFixtures\CategorieFixtures;
use App\DataFixtures\GlobalFixtures;
use App\Entity\Article;
use App\Entity\Categorie;
use Symfony\Component\HttpFoundation\Response;

class UserControllerTest extends GlobalTest
{

    const TEST_TEST = 'testtest';
    const CONNEXION = 'Connexion';
    const DECONNEXION = 'Déconnexion';
    const CONNECTION = 'Connection';
    const INSCRIPTION = 'Inscription';
    const TESTTEST_EXEMPLE_FR = 'test@gmail.com';
    const TESTPASSWORD = 'testpassword';

    public function testLogin()
    {
        $this->client = static::createClient();
        $crawler = $this->client->request('GET', '/login');
        $this->assertSelectorTextContains('html h2', self::CONNEXION);
        $form = $crawler->selectButton(self::CONNECTION)->form();
        $form['email'] = 'admin@exemple.fr';
        $form['password'] = 'admin';
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorTextContains('html', self::DECONNEXION);
    }

    public function testDeconection()
    {
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/');

        $link = $crawler->selectLink('Déconnexion')->link();
        $this->client->click($link);
        $this->client->followRedirect();

        $this->assertSelectorTextContains('html', self::CONNEXION);
    }

    public function testInscription()
    {
        $this->client = static::createClient();
        $crawler = $this->client->request('GET', '/register');
        $this->assertSelectorTextContains('html h2', self::INSCRIPTION);
        $form = $crawler->selectButton(self::INSCRIPTION)->form();
        $form['registration_form[email]'] = self::TESTTEST_EXEMPLE_FR;
        $form['registration_form[password][first]'] = self::TESTPASSWORD;
        $form['registration_form[password][second]'] = self::TESTPASSWORD;
        $form['registration_form[agreeTerms]'] = 1;
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorTextContains('html', self::CONNEXION);
        $crawler = $this->client->request('GET', '/login');
        $form = $crawler->selectButton(self::CONNECTION)->form();
        $form['email'] = self::TESTTEST_EXEMPLE_FR;
        $form['password'] = self::TESTPASSWORD;
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorTextContains('html', self::DECONNEXION);
    }
}
