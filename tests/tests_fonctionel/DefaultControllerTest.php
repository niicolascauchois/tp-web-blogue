<?php

namespace App\Tests\tests_fonctionel;

use Symfony\Component\HttpFoundation\Response;

class DefaultControllerTest extends GlobalTest
{

    public function testIndex()
    {
        $this->client = static::createClient();
        $crawler = $this->client->request('GET', '/');

        $this->assertSelectorTextContains('html h1 a', 'Blog NicoCau');
        $this->assertSelectorTextContains('html h2', 'Derniers articles');
        $this->assertSame(8, $crawler->filter('div.article-card')->count());
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }
}
