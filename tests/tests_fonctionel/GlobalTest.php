<?php

namespace App\Tests\tests_fonctionel;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GlobalTest extends WebTestCase
{
    /**
     * @var KernelBrowser
     */
    protected $client;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();

        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function test():void {
        $this->assertTrue(true);
    }

    public function loginAdmin(){

        $this->client = static::createClient();
        $crawler = $this->client->request('GET', '/login');

        $form = $crawler->selectButton('Connection')->form();
        $form['email'] = 'admin@exemple.fr';
        $form['password'] = 'admin';
        $this->client->submit($form);
        $crawler = $this->client->followRedirect();
        return $crawler;
    }

    public function loginUser(){

        $this->client = static::createClient();
        $crawler = $this->client->request('GET', '/login');

        $form = $crawler->selectButton('Connection')->form();
        $form['email'] = 'test@exemple.fr';
        $form['password'] = 'test';
        $this->client->submit($form);
        $crawler = $this->client->followRedirect();
        return $crawler;
    }
}
