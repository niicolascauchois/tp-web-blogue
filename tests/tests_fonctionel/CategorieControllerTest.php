<?php

namespace App\Tests\tests_fonctionel;

use App\DataFixtures\CategorieFixtures;
use App\DataFixtures\GlobalFixtures;
use App\Entity\Categorie;
use Symfony\Component\HttpFoundation\Response;

class CategorieControllerTest extends GlobalTest
{

    const TEST_TEST = 'TestTest';
    const TEST_TEST_EDIT = 'TestTestEdit';

    public function testIndex()
    {
        $this->client = static::createClient();
        $crawler = $this->client->request('GET', '/categorie/');

        $this->assertSelectorTextContains('html h1 a', 'Blog NicoCau');
        $this->assertSelectorTextContains('html h2', 'Categories');
        $this->assertSame(CategorieFixtures::NB_CATEGORIE, $crawler->filter('a.categorie-card')->count());
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    public function testVieuw()
    {
        $this->client = static::createClient();
        $crawler = $this->client->request('GET', '/categorie/');

        $link = $crawler->filter('a.categorie-card')->eq(0)->link();
        $crawler = $this->client->click($link);

        $this->assertSelectorTextContains('html h1 a', 'Blog NicoCau');
        $titre = $this->client->getRequest()->getRequestUri();
        $titre = explode('/', $titre)[2];
        $this->assertSelectorTextContains('html h2', $titre);
    }

    public function testAdd()
    {
//        Test admin
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/');

        $link = $crawler->selectLink('Crée une catégorie')->link();
        $crawler = $this->client->click($link);

        $this->assertSelectorTextContains('html h1 a', 'Blog NicoCau');
        $this->assertSelectorTextContains('html h2', 'Crée une nouvelle categorie');

        $form = $crawler->selectButton('Enregistrer')->form();
        $form['categorie[nom]'] = self::TEST_TEST;
        $this->client->submit($form);

        $categorie = $this->entityManager
            ->getRepository(Categorie::class)
            ->findOneBy([], ['id' => 'DESC']);

        $this->assertEquals(self::TEST_TEST, $categorie->getNom());

//        Test non admin
        $this->loginUser();

        $crawler = $this->client->request('GET', '/');

        $link = $crawler->selectLink('Crée une catégorie')->link();
        $crawler = $this->client->click($link);

        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->client->getResponse()->getStatusCode());
    }

    public function testEdit()
    {
//        Test admin
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/categorie/' . self::TEST_TEST . '/edit');

        $this->assertSelectorTextContains('html h1 a', 'Blog NicoCau');
        $this->assertSelectorTextContains('html h2', 'Modifier : ' . self::TEST_TEST);

        $form = $crawler->selectButton('Valider')->form();
        $form['categorie[nom]'] = self::TEST_TEST_EDIT;
        $this->client->submit($form);

        $categorie = $this->entityManager
            ->getRepository(Categorie::class)
            ->findOneBy([], ['id' => 'DESC']);

        $this->assertEquals(self::TEST_TEST_EDIT, $categorie->getNom());

//        Test non admin
        $this->loginUser();

        $crawler = $this->client->request('GET', '/categorie/' . self::TEST_TEST_EDIT . '/edit');

        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->client->getResponse()->getStatusCode());
    }

    public function testDelette()
    {
//        Test admin
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/categorie/' . self::TEST_TEST_EDIT . '/edit');

        $this->assertSelectorTextContains('html h1 a', 'Blog NicoCau');
        $this->assertSelectorTextContains('html h2', 'Modifier : ' . self::TEST_TEST_EDIT);

        $form = $crawler->selectButton('Supprimer')->form();
        $form->disableValidation();
        $this->client->submit($form);

        $categorie = $this->entityManager
            ->getRepository(Categorie::class)
            ->findOneBy([], ['id' => 'DESC']);
        $categories = $this->entityManager
            ->getRepository(Categorie::class)
            ->findAll();

        $this->assertNotEquals(self::TEST_TEST, $categorie->getNom());
        $this->assertEquals(GlobalFixtures::NB_CATEGORIE, count($categories));
    }
}
