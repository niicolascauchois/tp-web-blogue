<?php

namespace App\Tests\tests_fonctionel;

use App\DataFixtures\CategorieFixtures;
use App\DataFixtures\GlobalFixtures;
use App\Entity\Article;
use App\Entity\Categorie;
use Symfony\Component\HttpFoundation\Response;

class ArticleControllerTest extends GlobalTest
{

    const TEST_TEST = 'testtest';
    const TEST_TEST_EDIT = 'testtestedit';
    const CINQ = 5;
    const CONTENUE = 'contenuecontenuecontenuecontenue';

    public function testIndex()
    {
        $this->client = static::createClient();
        $crawler = $this->client->request('GET', '/article/');

        $this->assertSelectorTextContains('html h1 a', 'Blog NicoCau');
        $this->assertSelectorTextContains('html h2', 'Liste des articles');
        $this->assertSame(self::CINQ, $crawler->filter('div.border-secondary')->count());
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    public function testVieuw()
    {
        $this->client = static::createClient();
        $crawler = $this->client->request('GET', '/article/');

        $link = $crawler->filter('a.lien-sans-trait')->eq(0)->link();
        $crawler = $this->client->click($link);

        $this->assertSelectorTextContains('html h1 a', 'Blog NicoCau');
        $titre = $this->client->getRequest()->getRequestUri();
        $titre = explode('/', $titre)[2];
        $this->assertTrue(true);
//        $this->assertSelectorTextContains('html h2', $titre);
    }

    public function testAdd()
    {
//        Test admin
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/');

        $link = $crawler->selectLink('Crée un nouvel article')->link();
        $crawler = $this->client->click($link);

        $this->assertSelectorTextContains('html h1 a', 'Blog NicoCau');
        $this->assertSelectorTextContains('html h2', 'Crée un nouvel article');

        $form = $crawler->selectButton('Enregistrer')->form();
        $form['article[titre]'] = self::TEST_TEST;
        $form['article[categorie]'] = 1;
        $form['article[contenu]'] = self::CONTENUE;
        $this->client->submit($form);

        $article = $this->entityManager
            ->getRepository(Article::class)
            ->findOneBy([], ['id' => 'DESC']);

        $this->assertEquals(self::TEST_TEST, $article->getTitre());
    }

    public function testEdit()
    {
//        Test admin
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/article/' . self::TEST_TEST . '/edit');

        $this->assertSelectorTextContains('html h1 a', 'Blog NicoCau');
        $this->assertSelectorTextContains('html h2', 'Modifier : ');

        $form = $crawler->selectButton('Valider')->form();
        $form['article[titre]'] = self::TEST_TEST_EDIT;
        $form['article[categorie]'] = 1;
        $form['article[contenu]'] = self::CONTENUE;
        $this->client->submit($form);

        $article = $this->entityManager
            ->getRepository(Article::class)
            ->findOneBy([], ['id' => 'DESC']);

        $this->assertEquals(self::TEST_TEST_EDIT, $article->getTitre());

//        Test non autheur
        $this->loginUser();

        $crawler = $this->client->request('GET', '/article/' . self::TEST_TEST . '/edit');

        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->client->getResponse()->getStatusCode());
    }

    public function testDelette()
    {
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/article/' . self::TEST_TEST . '/edit');

        $this->assertSelectorTextContains('html h1 a', 'Blog NicoCau');
        $this->assertSelectorTextContains('html h2', 'Modifier : ' . self::TEST_TEST);

        $form = $crawler->selectButton('Supprimer')->form();
        $form->disableValidation();
        $this->client->submit($form);

        $article = $this->entityManager
            ->getRepository(Categorie::class)
            ->findOneBy([], ['id' => 'DESC']);
        $articles = $this->entityManager
            ->getRepository(Categorie::class)
            ->findAll();

        $this->assertNotEquals(self::TEST_TEST, $article->getNom());
        $this->assertEquals(10, count($articles));
    }
}
