// Gestion de la possition du fouter
var footerPosition = function() {
    Array.prototype.slice.call(document.getElementsByTagName("footer")).forEach(function (element) {
        element.style.position = null;
        element.style.bottom = null;
        element.style.width = null;
    })
    if (document.getElementById("body").offsetHeight < window.innerHeight) {
        Array.prototype.slice.call(document.getElementsByTagName("footer")).forEach(function (element) {
            element.style.position = "absolute";
            element.style.bottom = "0";
            element.style.width = "100%";
        })
    }
};

$( window ).resize(footerPosition);
$( window ).ready(footerPosition);